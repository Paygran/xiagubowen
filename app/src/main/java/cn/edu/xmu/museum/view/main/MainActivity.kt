package cn.edu.xmu.museum.view.main

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.DisplayMetrics
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.helper.GsonUtil
import cn.bmob.v3.listener.FetchUserInfoListener
import cn.edu.xmu.museum.view.user.UserDetailFragment
import cn.edu.xmu.museum.App
import cn.edu.xmu.museum.R
import cn.edu.xmu.museum.model.User
import cn.edu.xmu.museum.utils.PermissionsHelper
import cn.edu.xmu.museum.utils.loadThumbnail
import cn.edu.xmu.museum.view.favorite.FavoritesFragment
import cn.edu.xmu.museum.view.museum.MuseumListFragment
import cn.edu.xmu.museum.view.user.LoginActivity
import cn.edu.xmu.museum.view.user.SignUpActivity
import com.makeramen.roundedimageview.RoundedImageView
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, OnFragmentTransition {
    override fun onTransition(title: String) {
        supportActionBar?.title = title
    }

    private var mFragment: Fragment? = null

    private lateinit var mAvatarView: RoundedImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

//        val toggle = ActionBarDrawerToggle(
//                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
//        drawer_layout.addDrawerListener(toggle)
//        toggle.syncState()
//
//        nav_view.setNavigationItemSelectedListener(this)
        initDrawer()
        init()
    }

    private fun initDrawer() {
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(this, drawerLayout,
                findViewById<Toolbar>(R.id.toolbar),
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        nav_view.setNavigationItemSelectedListener(this)
        val navHeaderView = nav_view.getHeaderView(0)
        mAvatarView = navHeaderView.findViewById(R.id.avatar_view)
        nav_view.menu.findItem(R.id.nav_museums).isChecked = true
    }

    private fun init() {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        App.screenHeight = displayMetrics.heightPixels
        App.screenWidth = displayMetrics.widthPixels
        nav_view.setCheckedItem(R.id.nav_museums)
    }

    private fun initFragment() {
        if (mFragment != null || supportFragmentManager.fragments.isNotEmpty()) {
            return
        }
        mFragment = MuseumListFragment.newInstance()
        supportFragmentManager.beginTransaction()
                .add(R.id.fragment, mFragment, MuseumListFragment.TAG)
                .commit()
    }

    override fun onResume() {
        super.onResume()
        initUser()
        checkPermissions()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (PermissionsHelper.hasCode(requestCode)) {
            if (PermissionsHelper.allPermissionsGranted(this)) {
                initFragment()
            }
        }
    }

    private fun checkPermissions() {
        PermissionsHelper.PERMISSIONS.forEach { permission->
            if (ContextCompat.checkSelfPermission(this, permission) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                    PermissionsHelper.showRequestDialog(this, permission, {
                        ActivityCompat.requestPermissions(this, arrayOf(permission), PermissionsHelper.getRequestCode(permission))
                    }, {
                        finish()
                    })
                } else {
//                    val intent = Intent()
//                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
//                    val uri = Uri.fromParts("package", packageName, null)
//                    intent.data = uri
//                    startActivity(intent)
                    ActivityCompat.requestPermissions(this, arrayOf(permission), PermissionsHelper.getRequestCode(permission))
                }
            }
        }
        if (PermissionsHelper.allPermissionsGranted(this)) {
            initFragment()
        }
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        menuInflater.inflate(R.menu.main, menu)
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        when (item.itemId) {
//            R.id.action_settings -> return true
//            else -> return super.onOptionsItemSelected(item)
//        }
//    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (nav_view.menu.findItem(id).isChecked) {
            drawerLayout.closeDrawer(GravityCompat.START)
            return true
        }
        when (item.itemId) {
            R.id.nav_favorites -> {
                mFragment = FavoritesFragment.newInstance()
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment, mFragment, FavoritesFragment.TAG)
                        .commit()
            }

            R.id.nav_museums -> {
                mFragment = supportFragmentManager.findFragmentByTag(MuseumListFragment.TAG)
                if (mFragment == null) {
                    mFragment = MuseumListFragment.newInstance()
                }
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment, mFragment, MuseumListFragment.TAG)
                        //.addToBackStack(null)
                        .commit()
            }

            R.id.nav_user_info -> {
                mFragment = supportFragmentManager.findFragmentByTag(UserDetailFragment.TAG)
                if (mFragment == null) {
                    mFragment = UserDetailFragment.newInstance()
                }
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment, mFragment, UserDetailFragment.TAG)
                        //.addToBackStack(null)
                        .commit()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private var mUser = BmobUser.getCurrentUser(User::class.java)

    fun initUser() {
        mUser = BmobUser.getCurrentUser(User::class.java)
        val navHeaderView = nav_view.getHeaderView(0)
        val notLoginContainer = navHeaderView.findViewById<View>(R.id.sign_in_and_sign_up)
        val usernameView = navHeaderView.findViewById<TextView>(R.id.username_view)
        val signInBtn = navHeaderView.findViewById<Button>(R.id.button_sign_in)
        val signUpBtn = navHeaderView.findViewById<Button>(R.id.button_sign_up)
        signInBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, LoginActivity::class.java)
            startActivity(intent)
        }
        signUpBtn.setOnClickListener {
            val intent = Intent(this@MainActivity, SignUpActivity::class.java)
            startActivity(intent)
        }
        if (mUser != null) {
            notLoginContainer.visibility = View.GONE
            usernameView.visibility = View.VISIBLE
            usernameView.text = mUser.username
            syncUser()
        } else {
            notLoginContainer.visibility = View.VISIBLE
            usernameView.visibility = View.GONE
            mAvatarView.setImageResource(R.drawable.ic_person)
            mFragment = supportFragmentManager.findFragmentByTag(MuseumListFragment.TAG)
            if (mFragment == null) {
                mFragment = MuseumListFragment.newInstance()
            }
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment, mFragment, MuseumListFragment.TAG)
                    //.addToBackStack(null)
                    .commit()
        }
    }

    private fun syncUser() {
        BmobUser.fetchUserJsonInfo(object : FetchUserInfoListener<String>() {
            override fun done(s: String?, e: BmobException?) {
                if (e == null) {
                    Log.i(TAG, s)
                    mUser = GsonUtil.toObject(s, User::class.java) as User
                    if (mUser.avatar != null) {
                        val thumbnail = BmobFile()
                        thumbnail.url = mUser.avatar!!.url
                        thumbnail.loadThumbnail(this@MainActivity, mUser.objectId) { imageFile ->
                            mAvatarView.setImageURI(Uri.fromFile(imageFile))
                        }
                    }
                } else {
                    Log.e(TAG, e.message)
                    if (e.errorCode == 9024) {
                        Log.e(TAG, "Not logged in")
                        val intent = Intent(this@MainActivity, LoginActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        })
    }

    companion object {
        private val TAG = MainActivity::class.simpleName
    }
}
