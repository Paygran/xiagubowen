package cn.edu.xmu.museum.model

import cn.bmob.v3.BmobObject
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.datatype.BmobGeoPoint
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.FindListener
import cn.bmob.v3.listener.QueryListener

data class Museum(
        var name: String = "",
        var introduction: String = "",
        var location: BmobGeoPoint? = null,
        var area: String? = "",
        var duration: String = "",
        var views: Int = 0,
        var ticket: Float = 0f
) : BmobObject()

fun getMuseumsRemote(pageSize: Int, pageNum: Int, onSuccess: (List<Museum>) -> Unit, onFailed: (Exception) -> Unit) {
    BmobQuery<Museum>()
            .setLimit(pageSize)
            .setSkip(pageNum * pageSize)
            .findObjects(object : FindListener<Museum>() {
                override fun done(result: MutableList<Museum>, e: BmobException?) {
                    if (e == null) {
                        if (result.isNotEmpty()) {
                            onSuccess(result)
                        } else {
                            onFailed(Exception("Empty result data"))
                        }
                    } else {
                        onFailed(e)
                    }
                }

            })
}

fun getMuseumById(id: String, onSuccess: (Museum) -> Unit, onFailed: (Exception) -> Unit) {
    BmobQuery<Museum>()
            .getObject(id, object : QueryListener<Museum>() {
                override fun done(museum: Museum?, e: BmobException?) {
                    if (e == null) {
                        if (museum != null) {
                            onSuccess(museum)
                        } else {
                            onFailed(Exception("Null result data."))
                        }
                    } else {
                        onFailed(e)
                    }
                }
            })
}

class EmptyResultException : Exception() {
    override val message: String?
        get() = "Empty Result Data!"
}