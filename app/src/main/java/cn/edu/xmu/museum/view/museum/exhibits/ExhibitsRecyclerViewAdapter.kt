package cn.edu.xmu.museum.view.museum.exhibits

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import cn.bmob.v3.BmobUser
import cn.edu.xmu.museum.App
import cn.edu.xmu.museum.R
import cn.edu.xmu.museum.model.*
import cn.edu.xmu.museum.utils.DecodeImageFileTaskManager
import cn.edu.xmu.museum.utils.loadThumbnail
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.write

class ExhibitsRecyclerViewAdapter(private val mMuseumId: String): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mExhibitsList = mutableListOf<Exhibits>()
    private val mExhibitsImages = mutableMapOf<String, MutableList<ExhibitsImg>>()
    private val decodeTask = DecodeImageFileTaskManager()
    private val mLikeMap = mutableMapOf<String, Boolean>()
    var onLikeClickListener: (Int, String, ImageView) -> Unit = { _, _, _ ->}

    fun getExhibitImages(favorite: Boolean) {
        getExhibits(mMuseumId, 20, 0, { exhibitsList ->
            if (exhibitsList.isNotEmpty()) {
                mExhibitsList.addAll(exhibitsList)
                if (favorite) {
                    val lock = ReentrantReadWriteLock()
                    mExhibitsList.forEach { exhibits ->
                        val rl = lock.readLock()
                        rl.lock()
                        val user = BmobUser.getCurrentUser(User::class.java)
                        getUserLikes(user, exhibits, { likes ->
                            if (likes.isNullOrEmpty()) {
                                try {
                                    mExhibitsList.remove(exhibits)
                                } finally {
                                    rl.unlock()
                                }
                            }
                        }, {
                            try {
                                mExhibitsList.remove(exhibits)
                            } finally {
                                rl.unlock()
                            }
                        })
                    }
                    lock.write {
                        notifyDataSetChanged()
                    }
                } else {
                    notifyDataSetChanged()
                }
            } else {
                Log.e(TAG, "Empty Exhibits Result, id: $mMuseumId")
            }
        }, { e ->
            Log.e(TAG, e.message)
        })
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_exhibis, parent, false)
        return ExhibitsViewHolder(v)
    }

    override fun getItemCount(): Int {
        return mExhibitsList.size
    }

    private fun setFavoriteDisplay(id: String, btn: ImageView) {
        val user = BmobUser.getCurrentUser(User::class.java)
        val exhibits = Exhibits()
        exhibits.objectId = id
        getUserLikes(user, exhibits, { userLikes ->
            if (userLikes.isNullOrEmpty()) {
                mLikeMap[exhibits.objectId] = false
                btn.setImageResource(R.drawable.ic_favorite_white)
            } else {
                mLikeMap[exhibits.objectId] = true
                btn.setImageResource(R.drawable.ic_favorite_red)
            }
        }, {
            mLikeMap[exhibits.objectId] = false
            btn.setImageResource(R.drawable.ic_favorite_white)
        })
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val h = holder as ExhibitsViewHolder
        val exhibits = mExhibitsList[position]
        h.exhibitsIntroView.text = exhibits.name
        if (mExhibitsImages[exhibits.objectId] != null) {
            val exhibitsImg = mExhibitsImages[exhibits.objectId]!![0]
            exhibitsImg.img.loadThumbnail(holder.itemView.context, exhibitsImg.objectId, App.screenWidth, App.screenWidth) { file ->
                decodeTask.decode(file) {
                    h.progressView.visibility = View.GONE
                    h.exhibitsImgView.visibility = View.VISIBLE
                    h.exhibitsImgView.setImageBitmap(it)
                }
            }
            setFavoriteDisplay(exhibits.objectId, holder.btnFavorite)

            holder.btnFavorite.setOnClickListener {
                onLikeClickListener(position, exhibits.objectId, holder.btnFavorite)
                //setFavoriteDisplay(exhibits.objectId, holder.btnFavorite)
            }
        } else {
            getExhibitsImages(exhibits, 10, 0, { exhibitsImages ->
                mExhibitsImages[exhibits.objectId] = exhibitsImages.toMutableList()
                val img = mExhibitsImages[exhibits.objectId]!![0]
                img.img.loadThumbnail(holder.itemView.context, img.objectId, App.screenWidth, App.screenWidth) { file ->
                    decodeTask.decode(file) {
                        h.progressView.visibility = View.GONE
                        h.exhibitsImgView.visibility = View.VISIBLE
                        h.exhibitsImgView.setImageBitmap(it)
                    }
                }
                setFavoriteDisplay(exhibits.objectId, holder.btnFavorite)

                holder.btnFavorite.setOnClickListener {
                    onLikeClickListener(position, exhibits.objectId, holder.btnFavorite)
                    //setFavoriteDisplay(exhibits.objectId, holder.btnFavorite)
                }
            }, {
                Log.e(TAG, it.message)
            })
        }
    }

    private class ExhibitsViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val exhibitsIntroView = itemView.findViewById<TextView>(R.id.exhibitsNameView)!!
        val exhibitsImgView = itemView.findViewById<ImageView>(R.id.exhibitsImageView)!!
        val progressView = itemView.findViewById<ProgressBar>(R.id.progressBar)!!
        val btnFavorite = itemView.findViewById<ImageView>(R.id.btnFavorite)!!
    }

    companion object {
        private val TAG = ExhibitsRecyclerViewAdapter::class.simpleName
    }
}