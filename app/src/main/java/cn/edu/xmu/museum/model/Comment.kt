package cn.edu.xmu.museum.model

import cn.bmob.v3.BmobObject
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobPointer
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.FindListener

data class Comment(
        var user: BmobUser,
        var museum: Museum,
        var comment: String
) : BmobObject()

fun getCommentsByUser(user: BmobUser, pageSize: Int, pageNum: Int,
                      onSuccess: (List<Comment>) -> Unit, onFailed: (Exception) -> Unit) {
    BmobQuery<Comment>()
            .addWhereEqualTo("user", BmobPointer(user))
            .setLimit(pageSize)
            .setSkip(pageSize * pageNum)
            .findObjects(object : FindListener<Comment>() {
                override fun done(comments: MutableList<Comment>, e: BmobException?) {
                    if (e == null) {
                        if (comments.isNotEmpty()) {
                            onSuccess(comments)
                        } else {
                            onFailed(EmptyResultException())
                        }
                    } else {
                        onFailed(e)
                    }
                }
            })
}

fun getCommentsByMuseum(museum: Museum, pageSize: Int, pageNum: Int,
                        onSuccess: (List<Comment>) -> Unit, onFailed: (Exception) -> Unit) {
    BmobQuery<Comment>()
            .addWhereEqualTo("museum", BmobPointer(museum))
            .setLimit(pageSize)
            .setSkip(pageSize * pageNum)
            .findObjects(object : FindListener<Comment>() {
                override fun done(comments: MutableList<Comment>, e: BmobException?) {
                    if (e == null) {
                        if (comments.isNotEmpty()) {
                            onSuccess(comments)
                        } else {
                            onFailed(EmptyResultException())
                        }
                    } else {
                        onFailed(e)
                    }
                }
            })
}