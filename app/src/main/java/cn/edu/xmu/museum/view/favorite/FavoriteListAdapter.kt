package cn.edu.xmu.museum.view.favorite

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.QueryListener
import cn.edu.xmu.museum.R
import cn.edu.xmu.museum.model.Museum
import cn.edu.xmu.museum.model.getMuseumImages
import cn.edu.xmu.museum.utils.DecodeImageFileTaskManager
import cn.edu.xmu.museum.utils.loadThumbnail
import cn.edu.xmu.museum.view.museum.exhibits.ExhibitsRecyclerViewAdapter
import com.makeramen.roundedimageview.RoundedImageView

class FavoriteListAdapter(private val museums: MutableList<Museum>,
                          private val onItemClick: (Int, String, ImageView) -> Unit = {_, _, _ ->}): RecyclerView.Adapter<FavoriteListAdapter.FavoriteViewHolder>() {
    private val decodeTask = DecodeImageFileTaskManager()

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        val museum = museums[position]
        BmobQuery<Museum>().getObject(museum.objectId, object: QueryListener<Museum>() {
            override fun done(m: Museum?, e: BmobException?) {
                if (e == null) {
                    holder.museumTitleView.text = m?.name
                }
            }
        })
        //holder.museumTitleView.text = museum.name
        getMuseumImages(1, 0, museum.objectId, {
            it[0].img.loadThumbnail(holder.itemView.context, it[0].objectId, 100, 100) { imageFile ->
                decodeTask.decode(imageFile) { bmp ->
                    holder.museumImageView.setImageBitmap(bmp)
                }
            }
        }, {

        })
        val adapter = ExhibitsRecyclerViewAdapter(museum.objectId)
        adapter.onLikeClickListener = onItemClick
        adapter.getExhibitImages(true)
        holder.museumFavorites.layoutManager = LinearLayoutManager(holder.itemView.context)
        holder.museumFavorites.adapter = adapter
    }

    override fun getItemCount(): Int {
        return museums.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_favorite, parent, false)
        return FavoriteViewHolder(v)
    }

    class FavoriteViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val museumImageView = itemView.findViewById<RoundedImageView>(R.id.museumImageView)
        val museumTitleView = itemView.findViewById<TextView>(R.id.museumTitleView)
        val museumFavorites = itemView.findViewById<RecyclerView>(R.id.museumFavorites)
    }
}