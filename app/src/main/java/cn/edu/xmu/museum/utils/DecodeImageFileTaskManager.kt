package cn.edu.xmu.museum.utils

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Handler
import android.os.Looper
import cn.edu.xmu.museum.view.museum.MuseumListFragment
import java.io.File

class DecodeImageFileTaskManager: TaskManager() {
    fun decode(file: File, onDone: (Bitmap) -> Unit) {
        mThreadPool.execute {
            val bmp = BitmapFactory.decodeFile(file.absolutePath)
            if (bmp != null) {
                Handler(Looper.getMainLooper()).post {
                    onDone(bmp)
                }
            }
        }
    }
}