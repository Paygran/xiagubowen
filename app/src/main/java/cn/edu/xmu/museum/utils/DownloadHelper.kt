package cn.edu.xmu.museum.utils

import android.content.Context
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.DownloadFileListener
import java.io.File
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

private const val TAG = "DownloadUtils"

private object CheckFileExistsTaskManager {
    private val mHandler = Handler(Looper.getMainLooper())

    private val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()

    private const val KEEP_ALIVE_TIME = 1L

    private val mDecodeWorkQueue: BlockingQueue<Runnable> = LinkedBlockingQueue()

    private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS

    private val mCheckFileThreadPool = ThreadPoolExecutor(
            NUMBER_OF_CORES,
            NUMBER_OF_CORES,
            KEEP_ALIVE_TIME,
            KEEP_ALIVE_TIME_UNIT,
            mDecodeWorkQueue
    )

    fun startCheck(file: File, bmobFile: BmobFile, onLoad: (File) -> Unit) {
        mCheckFileThreadPool.execute {
            if (file.exists()) {
                mHandler.post {
                    onLoad(file)
                }
            } else {
                downloadFile(file, bmobFile, onLoad)
            }
        }
    }
}

private fun downloadFile(cacheFile: File, bmobFile: BmobFile, onDownloadComplete: ((File) -> Unit)?) {
    bmobFile.download(cacheFile, object : DownloadFileListener() {
        override fun onProgress(p0: Int?, p1: Long) {
            //do nothing
            this.onFinish()
        }

        override fun done(savePath: String?, e: BmobException?) {
            if (e == null) {
                Log.i(TAG, "Cache $savePath Complete")
                onDownloadComplete?.invoke(File(savePath))
            } else {
                Log.e(TAG, "Download file unsuccessfully, check the network", e)
            }
        }

    })
}

fun BmobFile.fromFileOrNetwork(context: Context, relativePath: String, onLoad: (File) -> Unit) {
    val cacheFile = File(context.externalCacheDir.absolutePath + relativePath)
    CheckFileExistsTaskManager.startCheck(cacheFile, this, onLoad)
    Handler(Looper.getMainLooper()).postDelayed({
        downloadFile(cacheFile, this, onLoad)
    }, 1000)
}

fun BmobFile.loadThumbnail(context: Context, id: String, operation: (File) -> Unit) {
    val format = "png"
    val thumbnail = BmobFile()
    thumbnail.url = this.url + "!/fwfh/100x100/format/png"
    val path = "/thumbnails/${id}100x100.$format"
    thumbnail.fromFileOrNetwork(context, path, operation)
}

fun BmobFile.loadThumbnail(context: Context, id: String, width: Int, height: Int, onLoad: (File) -> Unit) {
    val format = "png"
    val thumbnail = BmobFile()
    thumbnail.url = this.url + "!/fwfh/${width}x$height/format/png"
    val path = "/thumbnails/$id${width}x$height.$format"
    thumbnail.fromFileOrNetwork(context, path, onLoad)
}

fun Uri.getFile(context: Context): File? {
    if (this.scheme == "file") return File(this.path)
    val projection = arrayOf(MediaStore.MediaColumns.DATA)
    val cursor = context.contentResolver.query(this, projection, null, null, null)
    val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
    cursor.moveToFirst()
    val path = cursor.getString(columnIndex)
    cursor.close()
    val file: File?
    if (path != null) {
        file = File(path)
    } else {
        return null
    }
    return if (file.exists()) file else null
}