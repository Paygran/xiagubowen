package cn.edu.xmu.museum.view.common

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class ItemMarginDecoration(private val space: Int) : RecyclerView.ItemDecoration() {
    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        if (parent.getChildAdapterPosition(view) <= 0) return
        outRect.top = space
    }
}