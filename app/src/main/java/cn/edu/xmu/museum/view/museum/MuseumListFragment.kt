package cn.edu.xmu.museum.view.museum

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.util.LruCache
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import cn.edu.xmu.museum.R
import cn.edu.xmu.museum.model.Museum
import cn.edu.xmu.museum.model.getMuseumImages
import cn.edu.xmu.museum.model.getMuseumsRemote
import cn.edu.xmu.museum.utils.TaskManager
import cn.edu.xmu.museum.utils.loadThumbnail
import cn.edu.xmu.museum.view.common.ItemMarginDecoration
import cn.edu.xmu.museum.view.common.PaginationScrollListener
import kotlinx.android.synthetic.main.fragment_museum_list.*
import java.io.File

class MuseumListFragment : Fragment() {
    private val mMuseums: MutableList<Museum?> = mutableListOf()
    private lateinit var mMuseumListAdapter: MuseumListAdapter
    private lateinit var mLayoutManager: LinearLayoutManager

    private var mCurrPage = -1
    private var isLoading = false
        set(value) {
            if (value == field) return
            if (value) {
                mMuseums.add(null)
                mMuseumListAdapter.notifyItemInserted(mMuseums.lastIndex)
            } else {
                if (mMuseums.isNotEmpty()) {
                    mMuseums.removeAt(mMuseums.lastIndex)
                    mMuseumListAdapter.notifyItemRemoved(mMuseums.size)
                }
            }
            field = value
        }

    private object DecodeFileTaskManager: TaskManager() {
        fun decode(file: File, onDone: (Bitmap) -> Unit) {
            if (!mThreadPool.isShutdown) {
                mThreadPool.execute {
                    val bmp = BitmapFactory.decodeFile(file.absolutePath)
                    if (bmp != null) {
                        Handler(Looper.getMainLooper()).post {
                            onDone(bmp)
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.app_name)
        DecodeFileTaskManager.newPool()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        //DecodeFileTaskManager.cancelAll()
    }

    override fun onPause() {
        super.onPause()
        DecodeFileTaskManager.cancelAll()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mMuseumListAdapter = MuseumListAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_museum_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mLayoutManager = LinearLayoutManager(context)
        museumRecyclerView.layoutManager = mLayoutManager
        museumRecyclerView.adapter = mMuseumListAdapter
        museumRecyclerView.addItemDecoration(ItemMarginDecoration(resources.getDimensionPixelSize(R.dimen.list_margin)))
        museumRecyclerView.addOnScrollListener(object : PaginationScrollListener(mLayoutManager) {
            override fun onLoadMore(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
                museumRecyclerView.post {
                    isLoading = true
                    getMuseumsRemote(PAGE_SIZE, mCurrPage + 1, {
                        isLoading = false
                        mCurrPage++
                        mMuseums.addAll(it)
                        mMuseumListAdapter.notifyDataSetChanged()
                    }, {
                        isLoading = false
                        Log.e(TAG, it.message)
                    })
                }
            }

            override fun isLoading(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): Boolean {
                return isLoading
            }

        })
        isLoading = true
        getMuseumsRemote(PAGE_SIZE, mCurrPage + 1, {
            isLoading = false
            mCurrPage++
            mMuseums.addAll(it)
            //mMuseumListAdapter.notifyItemRangeInserted(mMuseums.size - it.size - 1, it.size)
            mMuseumListAdapter.notifyDataSetChanged()
        }, {
            isLoading = false
            Log.e(TAG, it.message)
        })
    }

    companion object {
        val TAG = MuseumListFragment::class.simpleName
        private const val PAGE_SIZE = 20
        private const val VIEW_TYPE_MUSEUM = 0
        private const val VIEW_TYPE_LOADING = 1
        fun newInstance(): MuseumListFragment {
            return MuseumListFragment()
        }
    }

    private inner class MuseumListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
        private val mMemoryCache: LruCache<String, Bitmap>
        private val mImageMap: MutableMap<String, String> = mutableMapOf()

        init {
            val maxMemory = Runtime.getRuntime().maxMemory() / 1024
            val cacheSize = maxMemory.toInt() / 8
            mMemoryCache = object : LruCache<String, Bitmap>(cacheSize) {
                override fun sizeOf(key: String, bitmap: Bitmap): Int {
                    return bitmap.byteCount / 1024
                }
            }
        }

        override fun getItemViewType(position: Int): Int {
            if (mMuseums[position] == null) {
                return VIEW_TYPE_LOADING
            }
            return VIEW_TYPE_MUSEUM
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            if (viewType == VIEW_TYPE_MUSEUM) {
                val v = LayoutInflater.from(context).inflate(R.layout.item_museum_list, parent, false)
                return MuseumViewHolder(v)
            }
            return LoadingViewHolder(LayoutInflater.from(context).inflate(R.layout.item_loading, parent, false))
        }

        override fun getItemCount(): Int {
            return mMuseums.size
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val museum = mMuseums[position]
            if (holder is MuseumViewHolder && museum != null) {
                holder.nameView.text = museum.name
                holder.introView.text = museum.introduction
                holder.itemView.setOnClickListener {
                    val intent = Intent(context, MuseumDetailsActivity::class.java)
                    intent.putExtra(MuseumDetailsActivity.ARG_MUSEUM, museum.objectId)
                    intent.putExtra(MuseumDetailsActivity.ARG_MUSEUM_NAME, museum.name)
                    startActivity(intent)
                }
                val imageId = mImageMap[museum.objectId]
                if (imageId != null) {
                    val imageBmp = mMemoryCache[imageId]
                    if (imageBmp != null) {
                        holder.imageView.setImageBitmap(imageBmp)
                        holder.imageView.visibility = View.VISIBLE
                    }
                } else {
                    Thread {
                        getMuseumImages(1, 0, museum.objectId, { imgList ->
                            if (imgList.isNotEmpty()) {
                                val image = imgList[0]
                                val ctx = context
                                ctx ?:return@getMuseumImages
                                image.img.loadThumbnail(ctx, image.objectId, 500, 500) { imageFile ->
                                    //                                    Thread {
//                                        val bmp = BitmapFactory.decodeFile(it.absolutePath)
//                                        if (bmp != null) {
//                                            mImageMap[museum.objectId] = image.objectId
//                                            mMemoryCache.put(image.objectId, bmp)
//                                            Handler(Looper.getMainLooper()).post {
//                                                holder.imageView.setImageBitmap(bmp)
//                                            }
//                                        }
//                                    }.start()
                                    DecodeFileTaskManager.decode(imageFile) {
                                        mImageMap[museum.objectId] = image.objectId
                                        mMemoryCache.put(image.objectId, it)
                                        holder.imageView.setImageBitmap(it)
                                        holder.imageView.visibility = View.VISIBLE
                                    }
                                }
                            }
                        }, {
                            Log.e(TAG, "Failed to load image ${museum.objectId}, with message ${it.message}", it)
                        })
                    }.start()
                }
            }
        }

        inner class MuseumViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val nameView: TextView = itemView.findViewById(R.id.nameView)
            val introView: TextView = itemView.findViewById(R.id.introView)
            val imageView: ImageView = itemView.findViewById(R.id.imageView)
        }

        inner class LoadingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            val progressView: ProgressBar = itemView.findViewById(R.id.progressBar)
        }
    }
}