package cn.edu.xmu.museum.utils

import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

abstract class TaskManager {
    private val mDecodeWorkQueue: BlockingQueue<Runnable> = LinkedBlockingQueue()


    protected var mThreadPool = ThreadPoolExecutor(
            NUMBER_OF_CORES,
            NUMBER_OF_CORES,
            KEEP_ALIVE_TIME,
            KEEP_ALIVE_TIME_UNIT,
            mDecodeWorkQueue
    )

    companion object {
        private val NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors()
        private val KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS
        private const val KEEP_ALIVE_TIME = 1L
    }

    fun newPool() {
        mThreadPool = ThreadPoolExecutor(
                NUMBER_OF_CORES,
                NUMBER_OF_CORES,
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                mDecodeWorkQueue
        )
    }

    fun cancelAll() {
        mThreadPool.shutdown()
    }
}