package cn.edu.xmu.museum.model

import cn.bmob.v3.BmobObject
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.datatype.BmobPointer
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.FindListener
import cn.bmob.v3.listener.QueryListener

data class MuseumImg(var museum: Museum, var img: BmobFile) : BmobObject()

fun getMuseumImages(pageSize: Int, pageNum: Int, museumId: String, onDone: (List<MuseumImg>) -> Unit, onFailed: (Exception) -> Unit) {
    val museum = Museum()
    museum.objectId = museumId
    BmobQuery<MuseumImg>()
            .addWhereEqualTo("museum", BmobPointer(museum))
            .setLimit(pageSize)
            .setSkip(pageSize * pageNum)
            .findObjects(object : FindListener<MuseumImg>() {
                override fun done(images: MutableList<MuseumImg>, e: BmobException?) {
                    if (e == null) {
                        if (images.isNotEmpty()) {
                            onDone(images)
                        } else {
                            onFailed(Exception("Empty result!"))
                        }
                    } else {
                        onFailed(e)
                    }
                }
            })
}

fun getMuseumImageById(id: String, onDone: (MuseumImg) -> Unit, onFailed: (Exception) -> Unit) {
    BmobQuery<MuseumImg>()
            .getObject(id, object: QueryListener<MuseumImg>() {
                override fun done(img: MuseumImg?, e: BmobException?) {
                    if (e == null) {
                        if (img != null) {
                            onDone(img)
                        } else {
                            onFailed(EmptyResultException())
                        }
                    } else {
                        onFailed(e)
                    }
                }
            })
}