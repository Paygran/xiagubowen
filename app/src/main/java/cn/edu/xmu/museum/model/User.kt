package cn.edu.xmu.museum.model

import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobFile

class User: BmobUser() {
    var avatar: BmobFile? = null
}