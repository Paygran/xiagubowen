package cn.edu.xmu.museum.view.common

import android.support.design.widget.AppBarLayout
import kotlin.math.abs

abstract class AppBarSateChangeListener: AppBarLayout.OnOffsetChangedListener {
    private var mCurrentSate = State.IDLE

    override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
        if (verticalOffset == 0) {
            if (mCurrentSate != State.EXPANDED) {
                onStateChanged(appBarLayout, State.EXPANDED)
            }
            mCurrentSate = State.EXPANDED
        } else if (abs(verticalOffset) >= appBarLayout.totalScrollRange) {
            if (mCurrentSate != State.COLLAPSED) {
                onStateChanged(appBarLayout, State.COLLAPSED)
            }
            mCurrentSate = State.COLLAPSED
        } else {
            if (mCurrentSate != State.IDLE) {
                onStateChanged(appBarLayout, State.IDLE)
            }
            mCurrentSate = State.IDLE
        }
    }

    abstract fun onStateChanged(appBarLayout: AppBarLayout, state: State)

    enum class State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }
}