package cn.edu.xmu.museum.view.favorite

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.BmobUser
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.QueryListener
import cn.edu.xmu.museum.R
import cn.edu.xmu.museum.model.*
import kotlinx.android.synthetic.main.fragment_favorites.*

class FavoritesFragment: Fragment() {
    private var myFavorites: Collection<String>? = null

    private val mMuseums: MutableList<Museum> = mutableListOf()

    private lateinit var mFavoriteListAdapter: FavoriteListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val sharedPref = activity?.getSharedPreferences(PREF_FAVOR_NAME, Context.MODE_PRIVATE) ?:return
        myFavorites = sharedPref.getStringSet(getString(R.string.preference_key_favorites), emptySet())
    }

    override fun onResume() {
        super.onResume()
        (activity as AppCompatActivity).supportActionBar?.setTitle(R.string.title_favorites)
    }

    private fun getMuseums() {
        val favorites = myFavorites.orEmpty()
        favorites.forEach {
            BmobQuery<Exhibits>()
                    .getObject(it, object: QueryListener<Exhibits>() {
                        override fun done(exhibits: Exhibits?, e: BmobException?) {
                            if (e == null && exhibits != null) {
                                if (mMuseums.count { it.objectId == exhibits.museum.objectId } <= 0) {
                                    mMuseums.add(exhibits.museum)
                                    mFavoriteListAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (myFavorites.orEmpty().isEmpty()) {
            tipsView.visibility = View.VISIBLE
        }
        mFavoriteListAdapter = FavoriteListAdapter(mMuseums) onClick@{ _, exhibitsId, imageView ->
            val exhibits = Exhibits()
            exhibits.objectId = exhibitsId
            val user = BmobUser.getCurrentUser(User::class.java)
            getUserLikes(user, exhibits, { userLikes ->
                var delete = false
                if (!userLikes.isNullOrEmpty()) {
                    delete = true
                }
                if (delete) {
                    setUserLikes(user, exhibits, {
                        imageView.setImageResource(R.drawable.ic_favorite_white)
                    }, {}, true)
                } else {
                    setUserLikes(user, exhibits, {
                        imageView.setImageResource(R.drawable.ic_favorite_red)
                    })
                }
            }, {
                setUserLikes(user, exhibits, {
                    imageView.setImageResource(R.drawable.ic_favorite_red)
                })
            })
        }
        favoriteList.layoutManager = LinearLayoutManager(context)
        favoriteList.adapter = mFavoriteListAdapter
        getMuseums()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    companion object {
        val TAG = FavoritesFragment::class.simpleName
        const val PREF_FAVOR_NAME = "favorites"
        fun newInstance(): FavoritesFragment {
            return FavoritesFragment()
        }
    }
}