package cn.edu.xmu.museum.utils

import android.content.Context
import com.amap.api.location.AMapLocation
import com.amap.api.location.AMapLocationClient
import com.amap.api.location.AMapLocationClientOption
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode
import com.amap.api.services.core.LatLonPoint
import com.amap.api.services.geocoder.*

class LocationHelper(private val context: Context) {
    private val mLocationClient = AMapLocationClient(context)
    private var mLocationOption: AMapLocationClientOption? = null

    fun getMyLocation(onGet: (AMapLocation) -> Unit) {
        mLocationOption = AMapLocationClientOption()
        mLocationOption?.isNeedAddress = true
        mLocationOption?.locationMode = AMapLocationMode.Hight_Accuracy
        mLocationOption?.interval = 2000
        mLocationClient.setLocationOption(mLocationOption)
        mLocationClient.setLocationListener {location ->

        }
        mLocationClient.startLocation()
    }
}

class LocationSearch(private val context: Context,
                     private val onSearch: (RegeocodeAddress?) -> Unit): GeocodeSearch.OnGeocodeSearchListener {
    override fun onRegeocodeSearched(result: RegeocodeResult?, code: Int) {
        if (code == 1000 && result != null) {
            onSearch(result.regeocodeAddress)
        } else {
            onSearch(null)
        }
    }

    override fun onGeocodeSearched(result: GeocodeResult?, code: Int) {

    }

    fun search(lat: Double, lon: Double) {
        val search = GeocodeSearch(context)
        search.setOnGeocodeSearchListener(this)
        val point = LatLonPoint(lat, lon)
        val query = RegeocodeQuery(point, 200f, GeocodeSearch.AMAP)
        search.getFromLocationAsyn(query)
    }

}