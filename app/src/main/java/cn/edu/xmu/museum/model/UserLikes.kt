package cn.edu.xmu.museum.model

import android.util.Log
import cn.bmob.v3.BmobObject
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.datatype.BmobPointer
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.FindListener
import cn.bmob.v3.listener.SaveListener
import cn.bmob.v3.listener.UpdateListener
import kotlin.Exception

data class UserLikes(
        var user: User? = null,
        var exhibits: Exhibits? = null
): BmobObject()

fun getUserLikes(user: User?, exhibits: Exhibits?, onDone: (List<UserLikes>) -> Unit, onFailed: (Exception) -> Unit) {
    val query = BmobQuery<UserLikes>()
    if (user != null) {
        query.addWhereEqualTo("user", BmobPointer(user))
    }
    if (exhibits != null) {
        query.addWhereEqualTo("exhibits", BmobPointer(exhibits))
    }
    query.findObjects(object: FindListener<UserLikes>() {
            override fun done(likes: MutableList<UserLikes>?, e: BmobException?) {
                if (e == null) {
                    if (likes.isNullOrEmpty()) {
                        onFailed(Exception("Like(s) is null or empty"))
                    } else {
                        onDone(likes)
                    }
                } else {
                    onFailed(e)
                }
            }
    })
}

fun setUserLikes(user: User, exhibits: Exhibits, onDone: (() -> Unit)? = null, onFailed: ((Exception) -> Unit)? = null, delete: Boolean = false) {
    if (delete) {
        getUserLikes(user, exhibits, {
            if (!it.isNullOrEmpty()) {
                it.forEach { like ->
                    val thisLike = UserLikes()
                    thisLike.objectId = like.objectId
                    thisLike.delete(object: UpdateListener() {
                        override fun done(e: BmobException?) {
                            if (e == null) {
                                Log.i(UserLikes::class.simpleName, "Delete user like successfully, id: ${like.objectId}")
                                onDone?.invoke()
                            } else {
                                Log.e(UserLikes::class.simpleName, "Delete user like failed, id: ${like.objectId}, msg: ${e.message}")
                                onFailed?.invoke(e)
                            }
                        }

                    })
                }
            }
        }, {})
        return
    }
    val like = UserLikes(user, exhibits)
    like.save(object: SaveListener<String?>() {
        override fun done(id: String?, e: BmobException?) {
            if (e != null) {
                onFailed?.invoke(Exception("Like failed, maybe network problem, user: ${user.objectId}, exhibits: ${exhibits.objectId}"))
            } else {
                onDone?.invoke()
            }
        }
    })
}
