package cn.edu.xmu.museum.model

import cn.bmob.v3.BmobObject
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.datatype.BmobPointer
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.FindListener

data class ExhibitsImg(
        var img: BmobFile,
        var exhibit: Exhibits = Exhibits()
) : BmobObject()

fun getExhibitsImages(exhibits: Exhibits, pageSize: Int, pageNum: Int,
                      onSuccess: (List<ExhibitsImg>) -> Unit, onFailed: (Exception) -> Unit) {
    BmobQuery<ExhibitsImg>()
            .addWhereEqualTo("exhibit", BmobPointer(exhibits))
            .setLimit(pageSize)
            .setSkip(pageSize * pageNum)
            .findObjects(object : FindListener<ExhibitsImg>() {
                override fun done(imageList: List<ExhibitsImg>, e: BmobException?) {
                    if (e == null) {
                        if (imageList.isNotEmpty()) {
                            onSuccess(imageList)
                        } else {
                            onFailed(EmptyResultException())
                        }
                    } else {
                        onFailed(e)
                    }
                }
            })
}

fun getExhibitsImages(exhibitsId: String, pageSize: Int, pageNum: Int,
                      onSuccess: (List<ExhibitsImg>) -> Unit, onFailed: (Exception) -> Unit) {
    val exhibits = Exhibits()
    exhibits.objectId = exhibitsId
    getExhibitsImages(exhibits, pageSize, pageNum, onSuccess, onFailed)
}