package cn.edu.xmu.museum.view.user

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.TextInputEditText
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobFile
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.SaveListener
import cn.bmob.v3.listener.UploadFileListener
import cn.edu.xmu.museum.R
import cn.edu.xmu.museum.model.User
import cn.edu.xmu.museum.utils.getFile
import com.makeramen.roundedimageview.RoundedImageView
import com.yalantis.ucrop.UCrop
import java.io.File

/**
 * A login screen that offers login via email/password.
 */
class SignUpActivity : AppCompatActivity() {

    private val TAG = SignUpActivity::class.simpleName

    private lateinit var mAvatarView: RoundedImageView
    private lateinit var mUsernameView: AutoCompleteTextView
    private lateinit var mPasswordView: EditText
    private lateinit var mProgressView: View
    private lateinit var mLoginFormView: View
    private lateinit var mEmailView: EditText
    private lateinit var mPhoneView: EditText
    private lateinit var mPasswordRepeatView: TextInputEditText

    private var mAvatarFile: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        setupActionBar()
        // Set up the login form.
        mPasswordRepeatView = findViewById(R.id.passwordRepeat)
        mAvatarView = findViewById(R.id.avatar_view)
        mAvatarView.setImageResource(R.drawable.ic_person)
        mAvatarView.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_PICK
            startActivityForResult(intent, ACTION_PICK_AVATAR)
        }

        mUsernameView = findViewById(R.id.username)

        mPasswordView = findViewById(R.id.password)
        mPasswordView.setOnEditorActionListener(TextView.OnEditorActionListener { _, id, _ ->
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptSignUp()
                return@OnEditorActionListener true
            }
            false
        })

        mEmailView = findViewById(R.id.email)

        mPhoneView = findViewById(R.id.phone_number)

        val mSignInButton = findViewById<Button>(R.id.sign_up_button)
        mSignInButton.setOnClickListener { attemptSignUp() }

        mLoginFormView = findViewById(R.id.login_form)
        mProgressView = findViewById(R.id.login_progress)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ACTION_PICK_AVATAR) {
                val uri = data?.data
                val destinationFileName = System.currentTimeMillis().toString() + ".jpg"
                val avatarDir = File(externalCacheDir.absolutePath, "avatar")
                Thread {
                    if (!avatarDir.exists()) {
                        avatarDir.mkdirs()
                    }
                    Handler(Looper.getMainLooper()).post {
                        if (uri != null) {
                            val options = UCrop.Options()
                            options.setCircleDimmedLayer(true)
                            options.setShowCropFrame(false)
                            UCrop.of(uri, Uri.fromFile(File(avatarDir, destinationFileName)))
                                    .withAspectRatio(1f, 1f)
                                    .withMaxResultSize(600, 600)
                                    .withOptions(options)
                                    .start(this@SignUpActivity)
                        }
                    }
                }.start()
            } else if (requestCode == UCrop.REQUEST_CROP) {
                if (data == null) return
                val resultImg = UCrop.getOutput(data)
                mAvatarView.setImageURI(resultImg)
                mAvatarFile = resultImg?.getFile(this)
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setupActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    private fun attemptSignUp() {

        // Reset errors.
        mUsernameView.error = null
        mPasswordView.error = null

        // Store values at the time of the login attempt.
        val username = mUsernameView.text.toString()
        val password = mPasswordView.text.toString()
        val phone = mPhoneView.text.toString()
        val email = mEmailView.text.toString()
        val passwordRepeat = mPasswordRepeatView.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) || !isPasswordValid(password)) {
            mPasswordView.error = getString(R.string.error_invalid_password)
            focusView = mPasswordView
            cancel = true
        }

        if (TextUtils.isEmpty(passwordRepeat) || !isPasswordCoincide()) {
            mPasswordRepeatView.error = getString(R.string.error_mismatch_password_repeat)
            focusView = mPasswordRepeatView
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.error = getString(R.string.error_field_required)
            focusView = mUsernameView
            cancel = true
        }

        if (TextUtils.isEmpty(phone)) {
            mPhoneView.error = getString(R.string.error_field_required)
            focusView = mPhoneView
            cancel = true
        } else if (!isPhoneValid(phone)) {
            mPhoneView.error = getString(R.string.error_invalid_phone)
            focusView = mPhoneView
            cancel = true
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.error = getString(R.string.error_field_required)
            focusView = mEmailView
            cancel = true
        } else if (!isEmailValid(email)) {
            mEmailView.error = getString(R.string.error_invalid_email)
            focusView = mEmailView
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView!!.requestFocus()
            return
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
        }

        val newUser = User()
        newUser.username = username
        newUser.setPassword(password)
        newUser.mobilePhoneNumber = phone
        newUser.email = email
        newUser.avatar = if (mAvatarFile != null) BmobFile(mAvatarFile) else null
        if (newUser.avatar != null) {
            newUser.avatar?.uploadblock(object : UploadFileListener() {

                override fun done(e: BmobException?) {
                    if (e == null) {
                        Log.i(TAG, "Upload avatarView successfully")
                        doSignUp(newUser)
                    } else {
                        Log.e(TAG, "Upload avatarView failed")
                        Toast.makeText(this@SignUpActivity, "上传头像失败", Toast.LENGTH_LONG).show()
                    }
                }
            })
        } else {
            doSignUp(newUser)
        }
    }

    private fun doSignUp(user: BmobUser) {
        user.signUp(object : SaveListener<User>() {
            override fun done(user: User?, e: BmobException?) {
                if (e == null) {
                    Toast.makeText(this@SignUpActivity, getString(R.string.toast_sign_up_success), Toast.LENGTH_LONG)
                            .show()
                    val intent = Intent(this@SignUpActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                } else {
                    Log.e(TAG, e.message)
                    when (e.errorCode) {
                        202 -> {
                            mUsernameView.error = getString(R.string.error_exist_username).format(mUsernameView.text.toString())
                        }
                        203 -> {
                            mEmailView.error = getString(R.string.error_exist_email).format(mEmailView.text.toString())
                        }
                        209 -> {
                            mPhoneView.error = getString(R.string.error_exist_phone).format(mPhoneView.text.toString())
                        }
                    }
                    showProgress(false)
                }
            }

        })
    }

    private fun isPasswordCoincide(): Boolean {
        val password = mPasswordView.text.toString()
        val passwordRepeat = mPasswordRepeatView.text.toString()
        return TextUtils.equals(password, passwordRepeat)
    }

    private fun isPhoneValid(phone: String): Boolean {
        val regExp = "^((13[0-9])|(15[^4])|(18[0235-9])|(17[0-8])|(147))\\d{8}$".toRegex()
        return regExp.matches(phone)
    }

    private fun isEmailValid(email: String): Boolean =
            email.contains("@")

    private fun isPasswordValid(password: String): Boolean =
            password.length > 4

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)

        mLoginFormView.visibility = if (show) View.GONE else View.VISIBLE
        mLoginFormView.animate().setDuration(shortAnimTime.toLong()).alpha(
                (if (show) 0 else 1).toFloat()).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                mLoginFormView.visibility = if (show) View.GONE else View.VISIBLE
            }
        })

        mProgressView.visibility = if (show) View.VISIBLE else View.GONE
        mProgressView.animate().setDuration(shortAnimTime.toLong()).alpha(
                (if (show) 1 else 0).toFloat()).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                mProgressView.visibility = if (show) View.VISIBLE else View.GONE
            }
        })
    }

    companion object {
        private const val ACTION_PICK_AVATAR: Int = 0
    }
}

