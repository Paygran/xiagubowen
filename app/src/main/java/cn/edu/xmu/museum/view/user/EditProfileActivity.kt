package cn.edu.xmu.museum.view.user

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import cn.bmob.v3.BmobUser
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.UpdateListener
import cn.edu.xmu.museum.R
import cn.edu.xmu.museum.model.User
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.content_edit_profile.*

class EditProfileActivity : AppCompatActivity() {
    private var doEdit: () -> Unit = {}
    private val mUser = BmobUser.getCurrentUser(User::class.java) as User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        when (intent.getIntExtra(UserDetailFragment.ARG_EDIT, UserDetailFragment.REQUEST_EDIT_USERNAME)) {
            UserDetailFragment.REQUEST_EDIT_USERNAME -> setEditUsername()
        }
    }

    private fun setEditUsername() {
        supportActionBar?.title = "修改用户名"
        editProfileView.setText(mUser.username)
        doEdit = {
            val updateUser = User()
            editProfileView.text.toString().trim().apply {
                if (this.isNotBlank()) {
                    updateUser.username = this
                    updateUser.update(mUser.objectId, object : UpdateListener() {
                        override fun done(e: BmobException?) {
                            if (e == null) {
                                Toast.makeText(this@EditProfileActivity, "修改用户名成功", Toast.LENGTH_SHORT)
                                        .show()
                                setResult(RESULT_SUCCESS)
                                finish()
                            } else {
                                Log.e(TAG, "Edit profile failed: ${e.message}")
                                Toast.makeText(this@EditProfileActivity, "修改用户名失败", Toast.LENGTH_LONG)
                                        .show()
                                setResult(RESULT_FAIL)
                                finish()
                            }
                        }
                    })
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_edit_profile, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_save -> doEdit()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private val TAG = EditProfileActivity::class.simpleName
        const val RESULT_SUCCESS = 100
        const val RESULT_FAIL = 101
    }
}
