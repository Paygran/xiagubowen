package cn.edu.xmu.museum.view.museum

import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import cn.bmob.v3.BmobUser
import cn.bmob.v3.datatype.BmobFile
import cn.edu.xmu.museum.R
import cn.edu.xmu.museum.model.*
import cn.edu.xmu.museum.utils.LocationSearch
import cn.edu.xmu.museum.utils.loadThumbnail
import cn.edu.xmu.museum.view.common.AppBarSateChangeListener
import cn.edu.xmu.museum.view.museum.exhibits.ExhibitsRecyclerViewAdapter
import kotlinx.android.synthetic.main.activity_museum_details.*
import kotlinx.android.synthetic.main.content_museum_details.*
import kotlinx.android.synthetic.main.fragment_header_image.*
import kotlinx.android.synthetic.main.fragment_museum_info.*

class MuseumDetailsActivity : AppCompatActivity() {
    private var museumId: String? = null
    private lateinit var museum: Museum
    private val mMuseumImages: ArrayList<MuseumImg> = arrayListOf()
    private lateinit var mImagesAdapter: MuseumImagesAdapter
//    private val mExhibitsList: ArrayList<Exhibits> = arrayListOf()
//    private val mExhibitsImages: ArrayList<ExhibitsImg> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_museum_details)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        init()
    }

    private fun setExpanded(expanded: Boolean) {
        val decor = window.decorView
        if (expanded) {
            decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.statusBarColor = Color.TRANSPARENT
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                decor.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            }
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                    window.statusBarColor = resources.getColor(R.color.colorPrimary, theme)
//                } else {
//                    window.statusBarColor = resources.getColor(R.color.colorPrimary)
//                }
//            }
        }
    }

    private fun init() {
        exhibitsView.isNestedScrollingEnabled = false
        exhibitsView.layoutManager = LinearLayoutManager(this)
        appbar.addOnOffsetChangedListener(object: AppBarSateChangeListener() {
            override fun onStateChanged(appBarLayout: AppBarLayout, state: State) {
                if (state == State.COLLAPSED) {
                    Log.i(TAG, "Collapsed!")
                    setExpanded(false)
                    supportActionBar?.setDisplayShowTitleEnabled(true)
                    theme.applyStyle(R.style.AppTheme, true)
                } else if (state == State.EXPANDED) {
                    Log.i(TAG, "Expanded!")
                    setExpanded(true)
                    supportActionBar?.setDisplayShowTitleEnabled(false)
                    toolbarLayout.isTitleEnabled = false
                }
            }

        })
        setExpanded(true)
        museumId = intent.getStringExtra(ARG_MUSEUM)
        toolbarLayout.title = intent.getStringExtra(ARG_MUSEUM_NAME)
        if (museumId == null) {
            finish()
            return
        }
        getMuseumById(museumId!!, { museum ->
            this.museum = museum
            getMuseumImages()
            val exhibitsAdapter = ExhibitsRecyclerViewAdapter(museumId!!)
            exhibitsAdapter.getExhibitImages(false)
            exhibitsAdapter.onLikeClickListener = onClick@{ _, exhibitsId: String, imageView: ImageView ->
                val exhibits = Exhibits()
                exhibits.objectId = exhibitsId
                val user = BmobUser.getCurrentUser(User::class.java)
                getUserLikes(user, exhibits, { userLikes ->
                    var delete = false
                    if (!userLikes.isNullOrEmpty()) {
                        delete = true
                    }
                    if (delete) {
                        setUserLikes(user, exhibits, {
                            imageView.setImageResource(R.drawable.ic_favorite_white)
                        }, {}, true)
                    } else {
                        setUserLikes(user, exhibits, {
                            imageView.setImageResource(R.drawable.ic_favorite_red)
                        })
                    }
                }, {
                    setUserLikes(user, exhibits, {
                        imageView.setImageResource(R.drawable.ic_favorite_red)
                    })
                })
                //setUserLikes(BmobUser.getCurrentUser(User::class.java), exhibits, null)
            }
            exhibitsView.adapter = exhibitsAdapter
            supportActionBar?.title = this.museum.name
            toolbarLayout.title = this.museum.name
            timeView.text = museum.duration
            feeView.text = if (museum.ticket == 0f) "免费" else museum.ticket.toString()
            locationView.text = "正在加载..."
            if (museum.introduction.trim().isBlank()) {
                museumIntroView.text = "暂无介绍..."
            } else {
                museumIntroView.text = museum.introduction
            }
            val location = LocationSearch(this) {
                locationView.text = it!!.formatAddress
            }
            if (this.museum.location != null) {
                location.search(this.museum.location!!.latitude, this.museum.location!!.longitude)
            }
        }, {
            Log.e(TAG, it.message)
            //finish()
        })
    }

    private fun getMuseumImages() {
        getMuseumImages(10, 0, museumId!!, {
            mMuseumImages.addAll(it)
            mImagesAdapter = MuseumImagesAdapter()
            museumImages.adapter = mImagesAdapter
        }, {
            Log.e(TAG, it.message)
        })
    }

    private inner class MuseumImagesAdapter : FragmentStatePagerAdapter(supportFragmentManager) {

        override fun getCount(): Int =
            mMuseumImages.size

        override fun getItem(position: Int): Fragment {
            val img = mMuseumImages[position]
            return MuseumImageFragment.newInstance(img.objectId, img.img.url)
        }
    }

    class MuseumImageFragment : Fragment() {
        private lateinit var mId: String
        private lateinit var mUrl: String

        private var mImageDecodeThread: Thread? = null

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            mId = arguments!!.getString(ARG_ID)
            mUrl = arguments!!.getString(ARG_URL)
        }

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            return inflater.inflate(R.layout.fragment_header_image, container, false)
        }

        override fun onDestroyView() {
            super.onDestroyView()
            val thread = mImageDecodeThread
            if (thread != null && thread.isAlive) {
                thread.interrupt()
            }
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val bmobFile = BmobFile()
            bmobFile.url = mUrl
            bmobFile.loadThumbnail(context!!, mId, 500, 500) {
                mImageDecodeThread = Thread{
                    val imgBmp = BitmapFactory.decodeFile(it.absolutePath)
                    Handler(Looper.getMainLooper())
                            .post {
                                if (museumImage != null) {
                                    museumImage.setImageBitmap(imgBmp)
                                }
                            }
                }
                mImageDecodeThread?.start()
            }
        }

        companion object {
            const val ARG_ID = "id"
            const val ARG_URL = "url"

            fun newInstance(id: String, url: String): MuseumImageFragment {
                val fragment = MuseumImageFragment()
                val arguments = Bundle()
                arguments.putString(ARG_ID, id)
                arguments.putString(ARG_URL, url)
                fragment.arguments = arguments
                return fragment
            }
        }
    }

    companion object {
        private val TAG = MuseumDetailsActivity::class.simpleName
        const val ARG_MUSEUM = "MuseumDetail"
        const val ARG_MUSEUM_NAME = "MuseumName"
    }
}
