package cn.edu.xmu.museum.view.common

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView

abstract class PaginationScrollListener(private val layoutManager: LinearLayoutManager)
    : RecyclerView.OnScrollListener() {
    private var visibleThreshold = 2

    abstract fun onLoadMore(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>)

    abstract fun isLoading(adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>): Boolean

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        if (dy <= 0) return
        val totalItemCount = layoutManager.itemCount
        if (!isLoading(recyclerView.adapter) &&
                totalItemCount <= layoutManager.findLastCompletelyVisibleItemPosition() + visibleThreshold) {
            onLoadMore(recyclerView.adapter)
        }
    }
}