package cn.edu.xmu.museum.model

import cn.bmob.v3.BmobObject
import cn.bmob.v3.BmobQuery
import cn.bmob.v3.datatype.BmobPointer
import cn.bmob.v3.exception.BmobException
import cn.bmob.v3.listener.FindListener

data class Exhibits(
        var name: String = "",
        var intro: String = "",
        var museum: Museum = Museum()
) : BmobObject()

fun getExhibits(museum: Museum, pageSize: Int, pageNum: Int,
                onSuccess: (List<Exhibits>) -> Unit, onFailed: (Exception) -> Unit) {
    BmobQuery<Exhibits>()
            .addWhereEqualTo("museum", BmobPointer(museum))
            .setLimit(pageSize)
            .setSkip(pageSize * pageNum)
            .findObjects(object : FindListener<Exhibits>() {
                override fun done(exhibitsList: MutableList<Exhibits>, e: BmobException?) {
                    if (e == null) {
                        if (exhibitsList.isNotEmpty()) {
                            onSuccess(exhibitsList)
                        } else {
                            onFailed(Exception("Empty result!"))
                        }
                    } else {
                        onFailed(e)
                    }
                }
            })
}

fun getExhibits(museumId: String, pageSize: Int, pageNum: Int,
                onSuccess: (List<Exhibits>) -> Unit, onFailed: (Exception) -> Unit) {
    val museum = Museum()
    museum.objectId = museumId
    getExhibits(museum, pageSize, pageNum, onSuccess, onFailed)
}