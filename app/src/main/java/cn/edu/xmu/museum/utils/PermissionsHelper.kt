package cn.edu.xmu.museum.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import cn.edu.xmu.museum.R

object PermissionsHelper {
    val PERMISSIONS = listOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_PHONE_STATE
    )

    private val PERMISSION_CODES = mapOf(
            PERMISSIONS[0] to 101,
            PERMISSIONS[1] to 102,
            PERMISSIONS[2] to 103
    )

    private val PERMISSION_MSG = mapOf(
            PERMISSIONS[0] to R.string.dialog_msg_storage,
            PERMISSIONS[1] to R.string.dialog_msg_location,
            PERMISSIONS[2] to R.string.dialog_msg_phone
    )

    const val REQ_CODE_CAMERA = 104

    fun hasCode(code: Int): Boolean =
        PERMISSION_CODES.values.contains(code)

    fun getRequestCode(permission: String): Int {
        return PERMISSION_CODES[permission] ?:100
    }

    fun showRequestDialog(context: Context, permission: String, onOk: () -> Unit, onCancel: () -> Unit) {
        AlertDialog.Builder(context)
                .setMessage(PERMISSION_MSG[permission] ?: R.string.dialog_msg_storage)
                .setPositiveButton(android.R.string.ok) { dialog, _ ->
                    onOk()
                    dialog.dismiss()
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    onCancel()
                    dialog.dismiss()
                }
                .show()
    }

    fun allPermissionsGranted(context: Context): Boolean {
        var granted = true
        PERMISSIONS.forEach { permission ->
            if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                granted = false
            }
        }
        return granted
    }
}